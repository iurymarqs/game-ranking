package com.iurymarqs.gameranking.service;

import java.util.List;

import com.iurymarqs.gameranking.data.PlayerDTO;

public interface IPlayerService {
    public List<PlayerDTO> getPlayers();

    public PlayerDTO getPlayer(int id);

    public void createPlayer(PlayerDTO player) throws Throwable;

    public void updatePlayer(PlayerDTO player);

    public void deletePlayer(int id);

    public PlayerDTO addMatch(int id) throws Throwable;

    public PlayerDTO addWin(int id) throws Throwable;
}