package com.iurymarqs.gameranking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

import com.iurymarqs.gameranking.data.ApiDTOBuilder;
import com.iurymarqs.gameranking.data.PlayerDTO;
import com.iurymarqs.gameranking.entities.Player;
import com.iurymarqs.gameranking.exceptions.MoreWinsThanMatchesException;
import com.iurymarqs.gameranking.repository.IPlayerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PlayerService implements IPlayerService {

    
	private IPlayerRepository playerRepository;

	@Autowired
	public PlayerService(IPlayerRepository playerRepository) {
		this.playerRepository = playerRepository;
	}
	
	@Override
	public List<PlayerDTO> getPlayers() {
        List<Player> entities = playerRepository.getPlayers();
        List<PlayerDTO> players = new ArrayList<PlayerDTO>();

        Iterator<Player> iterator = entities.iterator();

        while(iterator.hasNext()) {
            Player player = iterator.next();
            players.add(ApiDTOBuilder.playerToPlayerDTO(player));
        }

		return players;
	}

	@Override
	public PlayerDTO getPlayer(int id) {
    Player player = playerRepository.getPlayer(id);
		return ApiDTOBuilder.playerToPlayerDTO(player);
	}

	@Override
	public void createPlayer(PlayerDTO player) {
		if (player.getWins() > player.getMatches()) 
			throw new MoreWinsThanMatchesException("Player can't have more wins than matches");
        
		playerRepository.createPlayer(ApiDTOBuilder.playerDTOToPlayer(player));
	}

	@Override
	public void updatePlayer(PlayerDTO player) {
		playerRepository.updatePlayer(ApiDTOBuilder.playerDTOToPlayer(player));
	}

	@Override
	public void deletePlayer(int id) {
		playerRepository.deletePlayer(id);
	}

	@Override
	public PlayerDTO addMatch(int id) {
		PlayerDTO player = ApiDTOBuilder.playerToPlayerDTO(playerRepository.getPlayer(id));
		player.addMatch();
		
		playerRepository.updatePlayer(ApiDTOBuilder.playerDTOToPlayer(player));
		return player;
	}

	@Override
	public PlayerDTO addWin(int id) throws Throwable {
		PlayerDTO player = ApiDTOBuilder.playerToPlayerDTO(playerRepository.getPlayer(id));

		if (player.getMatches() <= player.getWins())
            throw new MoreWinsThanMatchesException("Player can't have more wins than matches");

		player.addWin();
		playerRepository.updatePlayer(ApiDTOBuilder.playerDTOToPlayer(player));
		return player;
	}

}