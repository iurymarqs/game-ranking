package com.iurymarqs.gameranking.application;

import java.util.List;

import javax.validation.Valid;

import com.iurymarqs.gameranking.data.PlayerDTO;
import com.iurymarqs.gameranking.service.IPlayerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/players")
@CrossOrigin(value="*")
public class PlayerController {

    @Autowired
    IPlayerService service;

    @RequestMapping(value="", method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<PlayerDTO>> readAll() {
        List<PlayerDTO> players = service.getPlayers();
        return new ResponseEntity<>(players, HttpStatus.OK);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET, produces="application/json")
    public ResponseEntity<PlayerDTO> read(@PathVariable int id) {
        PlayerDTO player = service.getPlayer(id);
        return new ResponseEntity<>(player, HttpStatus.OK);
    }

    @RequestMapping(value="", method=RequestMethod.POST, produces="application/json" )
    public ResponseEntity<PlayerDTO> create(@Valid @RequestBody PlayerDTO player) throws Throwable {
        service.createPlayer(player);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/addMatches", method=RequestMethod.PUT, produces="application/json")
    public ResponseEntity<PlayerDTO> addMatch(@PathVariable int id) throws Throwable {
        PlayerDTO player = service.getPlayer(id);
        player = service.addMatch(id);
        return new ResponseEntity<>(player, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/addWins", method=RequestMethod.PUT, produces="application/json")
    public ResponseEntity<PlayerDTO> addWin(@PathVariable int id) throws Throwable {
        PlayerDTO player = service.getPlayer(id);
        player = service.addWin(id);
        return new ResponseEntity<>(player, HttpStatus.OK);
    }
}