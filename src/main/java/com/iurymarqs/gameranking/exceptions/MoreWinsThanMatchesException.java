package com.iurymarqs.gameranking.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.BAD_REQUEST)
public class MoreWinsThanMatchesException extends RuntimeException {

  public MoreWinsThanMatchesException(String exception) {
    super(exception);
  }
}