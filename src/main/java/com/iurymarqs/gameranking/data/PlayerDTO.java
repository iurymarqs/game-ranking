package com.iurymarqs.gameranking.data;

public class PlayerDTO {

    private int id;
    private String name;
    private int matches;
    private int wins;

	public PlayerDTO() {};

	public PlayerDTO(String name, int matches, int wins) {
		this.name = name;
		this.matches = matches;
		this.wins = wins;
	}

	public PlayerDTO(int id, String name, int matches, int wins) {
			this.id = id;
			this.name = name;
			this.matches = matches;
			this.wins = wins;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the wins
	 */
	public int getWins() {
		return wins;
	}
	/**
	 * @param wins the wins to set
	 */
	public void setWins(int wins) {
		this.wins = wins;
	}
	/**
	 * @return the matches
	 */
	public int getMatches() {
		return matches;
	}
	/**
	 * @param matches the matches to set
	 */
	public void setMatches(int matches) {
		this.matches = matches;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public void addWin() {
		this.wins++;	
	}

	public void addMatch() {
		this.matches++;
	}
}