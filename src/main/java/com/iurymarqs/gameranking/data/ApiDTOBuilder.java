package com.iurymarqs.gameranking.data;

import com.iurymarqs.gameranking.entities.Player;

public class ApiDTOBuilder {
    public static PlayerDTO playerToPlayerDTO(Player player) {
        return new PlayerDTO(player.getId(), player.getName(), 
            player.getMatches(), player.getWins());
    }

    public static Player playerDTOToPlayer(PlayerDTO player) {
        return new Player(player.getId(), player.getName(), 
            player.getMatches(), player.getWins());
    }
}