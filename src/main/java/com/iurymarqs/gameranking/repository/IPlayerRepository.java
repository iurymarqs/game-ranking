package com.iurymarqs.gameranking.repository;

import java.util.List;

import com.iurymarqs.gameranking.entities.Player;

public interface IPlayerRepository {

    public List<Player> getPlayers();

    public Player getPlayer(int id);

    public void createPlayer(Player player);

    public void updatePlayer(Player player);

    public void deletePlayer(int id);
}