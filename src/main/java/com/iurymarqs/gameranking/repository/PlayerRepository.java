package com.iurymarqs.gameranking.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.iurymarqs.gameranking.entities.Player;

import org.springframework.stereotype.Repository;

@Repository
public class PlayerRepository implements IPlayerRepository {

    @PersistenceContext
    EntityManager em;

	@Override
    @Transactional
    public List<Player> getPlayers() {
        List<Player> results = em.createQuery("FROM Player ORDER BY wins DESC", Player.class).getResultList();
        return results;
	}

    @Override
    @Transactional    
	public Player getPlayer(int id) {
		return em.find(Player.class, id);
	}

    @Override
    @Transactional 
	public void createPlayer(Player player) {
		em.persist(player);
	}

    @Override
    @Transactional 
	public void updatePlayer(Player player) {
		em.merge(player);
	}

    @Override
    @Transactional 
	public void deletePlayer(int id) {
        Player player = this.getPlayer(id);
        em.remove(player);
	}

}