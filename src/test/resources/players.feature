Feature: Player features

  Scenario: It should update the player
    Given I have a player with 1 match
      When I call a method to add a match
      Then I receive a player with 2 matches
    
    Given I have a player with 1 wins and 2 matches
      When I call a method to add a win
      Then I receive a player with 2 wins

    Given I have a player with 2 wins and 2 matches
      When I call a method to add a win
      Then I receive a player with 2 wins