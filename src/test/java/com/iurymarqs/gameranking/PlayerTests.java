package com.iurymarqs.gameranking;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.iurymarqs.gameranking.data.PlayerDTO;
import com.iurymarqs.gameranking.entities.Player;
import com.iurymarqs.gameranking.exceptions.MoreWinsThanMatchesException;
import com.iurymarqs.gameranking.repository.IPlayerRepository;
import com.iurymarqs.gameranking.service.PlayerService;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PlayerTests {

  IPlayerRepository playerRepository = mock(IPlayerRepository.class);
  PlayerService playerService;

  Player player;
  PlayerDTO returnedPlayer;

  @Before
  public void setup() {
    this.playerService = new PlayerService(playerRepository);
  }

  @Given("I have a player with {int} wins and {int} matches")
  public void i_have_a_player_with_wins_and_matches(Integer wins, Integer matches) {
    player = new Player(0, "John Doe", matches, wins);
    when(playerRepository.getPlayer(0)).thenReturn(player);
  }

  @Given("I have a player with {int} match")
  public void I_have_a_player_with_match(Integer matches) {
    player = new Player(0, "John Doe", matches, 0);
    when(playerRepository.getPlayer(0)).thenReturn(player);
  }


  @When("I call a method to add a match")
  public void i_call_a_method_to_add_a_match() throws Throwable {
      returnedPlayer = this.playerService.addMatch(player.getId());
  }

  @Then("I receive a player with {int} matches")
  public void i_receive_a_player_with_matches(Integer matches) throws Throwable {
      System.out.println(matches);
      assertThat(this.returnedPlayer.getMatches()).isEqualTo(matches);
  }

  @When("I call a method to add a win")
  public void i_call_a_method_to_add_a_win() {
      try {
        returnedPlayer = this.playerService.addWin(player.getId());
      } catch (Throwable e) {
        assertThat(e).isInstanceOf(MoreWinsThanMatchesException.class);
      }
  }

  @Then("I receive a player with {int} wins")
  public void i_receive_a_player_with_wins(Integer wins) {
      assertThat(this.returnedPlayer.getWins()).isEqualTo(wins);
  } 
}